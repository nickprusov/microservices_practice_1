# An example microservices application with cote

This is an example microservices application developed with
[cote](https://github.com/dashersw/cote).

Run all the files on separate terminals and see how they
communicate with each other.

node conversion-client.js
node conversion-service.js
node arbitration-service.js
node arbitration-updater.js